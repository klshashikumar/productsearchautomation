#SHASHIKUMAR KL

# Product Search Automation

Product Search Automation will help to compare price among amazon and Flipkart.

I have Writen a simple Python Program to fetch all items from amazon without using any driver for browser(i.e Chromedriver/  geekodriver).

## Requirement

### Python packages
bs4 (beautiful soup)\
xlwt (To write into Excel Sheet)\
urllib (To access URL)


```bash
pip install xlwt
pip install bs4
```

## import all libraries as follow

```python
import bs4
import xlwt 
from urllib.request import urlopen
from bs4 import BeautifulSoup as soup 
from xlwt import Workbook 
import os
from os import path
```

## Class

```python

class ProdcutSearchAutomation:
    def __init__(self, search_element):
        self.search_element = search_element.replace(" ","%20")


```
Initialize the Class with Search Element and I have replaced " " with "%20" to make it perfect for searching in the browser.

## Searching Method  in Amazon

```python

    def searchAmazon(self):
        s = self.search_element
        amazon_url = 'https://www.amazon.in/s?k='+s+'&ref=nb_sb_noss_2'
        #opening the client connection and read the data from amazon page
        uAmazonClient = urlopen(amazon_url)
        Amazon_page_html = uAmazonClient.read()
        uAmazonClient.close()
        #column Names
        wb = Workbook() 
        # Creating a Sheet
        sheet1 = wb.add_sheet('Sheet 1') 
        sheet1.write(0,0,"Name of Product")
        sheet1.write(0,1,"Price of Product")
        #parser
        page_soup = soup(Amazon_page_html,"html.parser")

        #Grab all items
        continers = page_soup.find_all("div", {"class":"s-expand-height s-include-content-margin s-border-bottom"})
        #Count of the elements Found
        length = len(continers)

        #looping and Adding all value sto excel sheet
        for i in range(0,length):
            try:
                #print("Name:",continers[i].find("span",{"class":"a-size-base-plus a-color-base a-text-normal"}).text)
                name = continers[i].find("span",{"class":"a-size-base-plus a-color-base a-text-normal"}).text
            except:
                name = "unknown"
            if(continers[i].find("span",{"class":"a-price-whole"})==None):
                #print("Price: Unknown")
                price="unknown"
            else:
                #print("Price: ",continers[i].find("span",{"class":"a-price-whole"}).text)
                price = continers[i].find("span",{"class":"a-price-whole"}).text
            #print("Ratings: ",continers[i].find("span",{"class":"a-icon-alt"}).text)

            #add values one by one
            sheet1.write(i+1,0,name)
            sheet1.write(i+1,1,price)
            

        wb.save('amazon.xls') 

``` 
This above method will grab all the items from amazon and stores it in the excel sheet.


## Searching Method in Flipkart

```python
def searchFlipkart(self):
        #Flipkart part
        #-------------------------------------------------------------------------------------------------
        #Elements want to be searched
        s = self.search_element
        flip_kart_url = 'https://www.flipkart.com/search?q='+s+'&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off'

        #print(my_url)

        #opening the client connection and read the data from amazon page
        uClient2 = urlopen(flip_kart_url)
        page_html2 = uClient2.read()
        uClient2.close()

        # Creating a Workbook 
        wb2 = Workbook()
        # Creating a Sheet
        sheet_flip = wb2.add_sheet('Sheet 1')  
        #column Names
        sheet_flip.write(0,0,"Name of Product")
        sheet_flip.write(0,1,"Price of Product")

        #parser
        page_soup2 = soup(page_html2,"html.parser")

        #Grab all items
        continers2 = page_soup2.find_all("div", {"class":"IIdQZO _1R0K0g _1SSAGr"})
        #Count of the elements Found
        length = len(continers2)

        #looping and Adding all value sto excel sheet
        for i in range(0,length):
            try:
                #print("Name:",continers[i].find("span",{"class":"a-size-base-plus a-color-base a-text-normal"}).text)
                name = continers2[i].find("span",{"class":"_2B_pmu"}).text
            except:
                name = "unknown"
            if(continers2[i].find("span",{"class":"_1vC4OE"})==None):
                #print("Price: Unknown")
                price="unknown"
            else:
                #print("Price: ",continers[i].find("span",{"class":"a-price-whole"}).text)
                price = continers2[i].find("span",{"class":"_1vC4OE"}).text
            #print("Ratings: ",continers[i].find("span",{"class":"a-icon-alt"}).text)

            #add values one by one
            sheet_flip.write(i+1,0,name)
            sheet_flip.write(i+1,1,price)
            
        wb2.save('flipKart.xls')

```

## Print nth elements 

```python
def nth_product_only(self,n):
        s = self.search_element
        amazon_url = 'https://www.amazon.in/s?k='+s+'&ref=nb_sb_noss_2'
        uClient3 = urlopen(amazon_url)
        page_html2 = uClient3.read()
        uClient3.close()
        page_soup = soup(page_html2,"html.parser")
        continers = page_soup.find_all("div", {"class":"s-expand-height s-include-content-margin s-border-bottom"})
        try:
            name = continers[n].find("span",{"class":"a-size-base-plus a-color-base a-text-normal"}).text
        except:
            name = "unknown"
        if(continers[n].find("span",{"class":"a-price-whole"})==None):
            price="unknown"
        else:
            price = continers[n].find("span",{"class":"a-price-whole"}).text
        print(n+"th Item")
        print("_________________________")    
        print("Name:",name)
        print("Price:",price)
        print("_________________________") 
   
```

The above method will print the 10th element from the list.


## Comparing Flipkart Price with Amazon Price.
```python
def compare_flip_amazon(self,compareItem):
        s = compareItem.replace(" ","%20")
        amazon_url = 'https://www.amazon.in/s?k='+s+'&ref=nb_sb_noss_2'
        flip_kart_url = 'https://www.flipkart.com/search?q='+s+'&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off'
    
        uClient4 = urlopen(amazon_url)
        page_html = uClient4.read()
        uClient4.close()
        page_soup = soup(page_html,"html.parser")
        continers = page_soup.find_all("div", {"class":"s-expand-height s-include-content-margin s-border-bottom"})
        if(continers[0].find("span",{"class":"a-price-whole"})==None):
            price="unknown"
        else:
            price = continers[0].find("span",{"class":"a-price-whole"}).text
        print(price)

        uClient5 = urlopen(flip_kart_url)
        page_html2 = uClient5.read()
        uClient5.close()
        page_soup = soup(page_html2,"html.parser")
        continers = page_soup.find_all("div", {"class":"IIdQZO _1R0K0g _1SSAGr"})
        if(continers[0].find("span",{"class":"_1vC4OE"})==None):
            price2="unknown"
        else:
            price2 = continers[0].find("span",{"class":"_1vC4OE"}).text
        print(price2)
        
        #comparison
        if(price2 == price):
            print("Amazon and flipkart sale "+compareItem+" for same rate")
        elif(price2 > price):
            print("Amazon's cost for "+compareItem+" is Less!")
        else:
            print("Flipkart's cost for "+compareItem+" is Less!")





```
The Above method will compare the item price in amazon and Flipkart.

## Main Method

```python
if __name__ == "__main__":
    psa = ProdcutSearchAutomation("Wrist watch")
    psa.remove_excel_ifexists()

    #to grab all amazon product to excel
    psa.searchAmazon()
    #to grab all amazon product to excel
    psa.searchFlipkart()

    #to Display 10th product
    psa.nth_product_only(7)

    #compare price among amazon and flipkart
    psa.compare_flip_amazon("wrist watch")



 
```

## Running The Program

Install all required packages using pip / apt-get

```bash

python Automate.py

```



